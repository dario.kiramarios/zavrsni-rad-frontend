//za automatsko zatvaranje navbara na klik bilo gdje drugdje
$(document).ready(function () {
    $(document).on('click', function (event) {
        if (!$(event.target).closest('.navbar-collapse').length) {
            $('.navbar-collapse').collapse('hide');
        }
    });
});