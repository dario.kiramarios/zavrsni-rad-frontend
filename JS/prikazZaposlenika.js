const saveButton = $(`#spremiPromjene`);

//ucitavanje i prikazivanje svih zaposlenika iz LocalStorage-a
$(document).ready(function () {
    if (localStorage.length === 0) {
        $(`#myTable`).hide();
    } else {
        $(`#myTable`).show();
        $(`#praznaTablicaTekst`).hide();

        var values = [],
            keys = Object.keys(localStorage),
            i = keys.length,
            brojac = 0;

        while (i--) {
            brojac = brojac + 1;
            var item = JSON.parse(localStorage.getItem(keys[i]));
            item.brojRetka = brojac;
            values.push(item);
        }

        var table = $('#myTableTablica').DataTable({
            data: values,
            rowId: values.email,
            "createdRow": function (row, data, dataIndex) {
                $(row).attr("id", data.uniqueID);
            },
            dom: "<'row'<'col-sm-12 col-md-6'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'p>>",
            "language": {
                "lengthMenu": "Prikaži _MENU_ djelatnika po stranici",
                "infoEmpty": "Nema informacija",
                "search": "Pretraži",
                "paginate": {
                    "first": "Prva",
                    "last": "Zadnja",
                    "next": "Sljedeća",
                    "previous": "Prošla"
                }
            },
            "columnDefs": [
                { "orderable": false, "targets": [6, 7] }
            ],
            columns: [
                { data: "brojRetka" },
                { data: "ime" },
                { data: "prezime" },
                { data: "email" },
                { data: "adresa" },
                { data: "broj" },
                {
                    data: null,
                    render: function (data) {
                        return '<button class="btn btn-primary btn-izmijeni">Detalji</button>';
                    }
                },
                {
                    data: null,
                    render: function (data) {
                        return '<button class="btn btn-danger btn-obrisi">Obriši</button>';
                    }
                }
            ]
        })
    }

    $(document).on('click', '.btn-izmijeni', function () {
        const rowData = table.row($(this).closest('tr')).data();
        let brojRedkaKojiSeBrise = rowData.brojRetka - 1;

        const imeValue = rowData.ime;
        const prezimeValue = rowData.prezime;
        const emailValue = rowData.email;
        const adresaValue = rowData.adresa;
        const brojValue = rowData.broj;
        const uniqueID = rowData.uniqueID;

        $('.is-valid').removeClass('is-valid');
        $('.is-invalid').removeClass('is-invalid');

        var myModal = new bootstrap.Modal($("#myModal"));
        myModal.toggle();

        $(`#myModal`).attr("data-redak", brojRedkaKojiSeBrise);
        $(`#myModal`).attr("data-uniqueID", uniqueID);
        $(`.modal-title`).html("Djelatnik " + imeValue + " " + prezimeValue);
        $(`#modal-ime`).val(imeValue);
        $(`#modal-prezime`).val(prezimeValue);
        $(`#modal-email`).val(emailValue);
        $(`#modal-adresa`).val(adresaValue);
        $(`#modal-brojMobitela`).val(brojValue);
    });

    $(document).on('click', '.btn-obrisi', function () {
        const rowData = table.row($(this).closest('tr')).data();
        var tablica = $('#myTableTablica').DataTable();
        let brojRedkaKojiSeBrise = rowData.brojRetka - 1;
        const uniqueID = rowData.uniqueID;
        var redakZaObrisati = tablica.row("#" + uniqueID);

        if (window.confirm("Sigurno želite obrisati djelatnika?")) {
            localStorage.removeItem(uniqueID)
            redakZaObrisati.remove();
            popraviIndekse(brojRedkaKojiSeBrise);
            const trenutnaStranica = tablica.page();
            tablica.draw(false);
            tablica.page(trenutnaStranica).draw(false);
        }
        if (localStorage.length === 0) {
            $(`#myTable`).hide();
            $(`#praznaTablicaTekst`).show();
        }
    });

})

//toast ako je uspjesno updatean djelatnik
function showToast() {
    const toast = new bootstrap.Toast(myToast);
    toast.show();
    //sakrij toast nakon 2.5s
    setTimeout(function () {
        toast.hide();
    }, 2500);
};

saveButton.on("click", function () {
    if (window.confirm("Jeste li sve podatke dobro upisali?")) {

        //ako ima dvostrukog razmaka zamijeni sa jednostrukim;
        //ako ima " ,", zamijeni sa ","
        const ime = $(`#modal-ime`).val().replace(/ {2,}/g, " ").trim();
        const prezime = $(`#modal-prezime`).val().replace(/ {2,}/g, " ").trim();
        const email = $(`#modal-email`).val().trim();
        const adresa = $(`#modal-adresa`).val().replace(/ {2,}/g, " ").replace(/ ,/g, ",").trim();
        const broj = $(`#modal-brojMobitela`).val();
        const redakBroj = $(`#myModal`).attr("data-redak")
        const uniqueID = $(`#myModal`).attr("data-uniqueid");

        const osoba = {
            ime,
            prezime,
            email,
            adresa,
            broj,
            uniqueID
        };

        const osobaJSON = JSON.stringify(osoba);
        localStorage.setItem(uniqueID, osobaJSON);

        showToast("myToast");
        $(`#myModal`).modal("hide");

        var tablica = $('#myTableTablica').DataTable();
        const redak = tablica.row(redakBroj).node();

        tablica.cell(redak, 1).data(ime);
        tablica.cell(redak, 2).data(prezime);
        tablica.cell(redak, 3).data(email);
        tablica.cell(redak, 4).data(adresa);
        tablica.cell(redak, 5).data(broj);

        const trenutnaStranica = tablica.page();
        tablica.draw(false);
        tablica.page(trenutnaStranica).draw(false);
    }
});

function popraviIndekse(redak) {
    var tablica2 = $(`#myTableTablica`).DataTable();

    tablica2.rows().every(function () {
        var rowData = this.data();
        if (rowData.brojRetka > redak) {
            rowData.brojRetka = rowData.brojRetka - 1; // Update the first column (#) in the row data
        }
        this.data(rowData); // Set the updated row data
    });
}