$(document).ready(function () {
    function updateButtonClass() {
        var windowWidth = $(window).width();

        if (windowWidth < 800) {
            $(".btn").addClass("btn-sm");
        } else {
            $(".btn").removeClass("btn-sm");
        }
    }

    //inicijalni poziv pri ucitavanju stranice
    updateButtonClass();
    //smanjiGumbice tijekom mijenjanja rezolucija
    $(window).resize(function () {
        updateButtonClass();
    });

    var tablica = $(`#myTableTablica`).DataTable();
    //kada se opet crta tablica, ako treba smanji gumbice
    tablica.on("draw", function () {
        var windowWidth = $(window).width();
        if (windowWidth < 800) {
            $(".btn").addClass("btn-sm");
        } else {
            $(".btn").removeClass("btn-sm");
        }
    })
});