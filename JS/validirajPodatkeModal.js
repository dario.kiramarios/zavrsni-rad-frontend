$.fn.isValid = function () {
    return this[0].checkValidity()
}

const myForm = $(`#formaModal`);
const spremiPromjene = $(`#spremiPromjene`);
const inputFields = myForm.find(`.form-control`);

$(document).ready(function () {
    const imeInput = $(`#modal-ime`);
    const prezimeInput = $(`#modal-prezime`);
    const emailInput = $(`#modal-email`);
    const adresaInput = $(`#modal-adresa`);
    const brojInput = $(`#modal-brojMobitela`);

    imeInput.on("input", function () {
        const regexBroj = /\d/;
        // zamijeni dvostruke razmake sa jednostrukim i makni razmak na pocetku i kraju imena
        const imeValue = imeInput.val().replace(/ {2,}/g, " ").trim();
        validateInputJQuery(imeInput)

        if (regexBroj.test(imeInput.val())) {
            $(`#imeValidation`).html("Potrebno je upisati ime bez znamenki");
            imeInput.addClass("is-invalid");
            imeInput.removeClass("is-valid");
        }
        else {
            $(`#imeValidation`).html("Potrebno je upisati ime sa barem 2 slova");
        }
        //u slucaju da su upisani razmaci pa da se to ne prihvati
        if (imeValue.length === 0) {
            $(`#imeValidation`).html("Potrebno je upisati ime sa barem 2 slova");
            imeInput.addClass("is-invalid");
            imeInput.removeClass("is-valid");
        }
        updateSubmitButtonStatus();
    });

    prezimeInput.on("input", function () {
        const regexBroj = /\d/;
        const prezimeValue = prezimeInput.val().replace(/ {2,}/g, " ").trim();
        validateInputJQuery(prezimeInput);

        if (regexBroj.test(prezimeInput.val())) {
            prezimeInput.addClass("is-invalid");
            prezimeInput.removeClass("is-valid");
            $(`#prezimeValidacija`).html("Potrebno je upisati prezime bez znamenki");
        }
        else {
            $(`#prezimeValidacija`).html("Potrebno je upisati prezime sa barem 2 slova");
        }
        if (prezimeValue.length === 0) {
            $(`#prezimeValidation`).html("Potrebno je upisati ime sa barem 2 slova");
            prezimeInput.addClass("is-invalid");
            prezimeInput.removeClass("is-valid");
        }
        updateSubmitButtonStatus();
    });

    emailInput.on("input", function () {
        const uniqueID = $(`#myModal`).attr("data-uniqueID");

        var validRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
        var emailValue = emailInput.val().trim();
        var zauzetEmail = provjeriEmailDostupnost(emailValue);

        if (zauzetEmail === false) {
            if (emailInput.isValid() === false || !emailValue.match(validRegex)) {
                $("#emailAdresa").html("Potrebno je upisati adresu oblika ime@domena");
            }
            validateInputJQuery(emailInput);
        }
        else {
            if (zauzetEmail !== uniqueID) {
                emailInput.removeClass("is-valid");
                emailInput.addClass("is-invalid");
                $("#emailAdresa").html("Ta email adresa je već zauzeta");
            }
            else {
                emailInput.addClass("is-valid");
                emailInput.removeClass("is-invalid");
            }
        }
        if (emailValue.length === 0) {
            emailInput.addClass("is-invalid");
            emailInput.removeClass("is-valid");
            $("#emailAdresa").html("Potrebno je upisati adresu oblika email@domena");
        }
        updateSubmitButtonStatus();
    });

    adresaInput.on("input", function () {
        //format oblika "ulica, grad" - zarez i minimalno 2 slova prije i poslije zareza su obavezni
        const regexAdresa = /^([\p{L}\d\s]{2,}),\s?([\p{L}\d\s]{2,})$/u;
        var adresaValue = adresaInput.val().split(",");

        var vrijediLiAdresa = provjeriAdresu(adresaValue);

        if (validateInputJQuery(adresaInput) === false || !regexAdresa.test(adresaInput.val()) || vrijediLiAdresa[0] === false) {
            adresaInput.addClass("is-invalid");
            adresaInput.removeClass("is-valid");
            $(`#adresaValidacija`).html("Potrebno je upisati adresu oblika \"Ulica, Grad\" i mora imati najmanje 4 znaka");
        }
        updateSubmitButtonStatus();
    });

    brojInput.on("input", function () {
        const uniqueID = $(`#myModal`).attr("data-uniqueID");
        const slobodanBroj = provjeriIdenticanBroj(brojInput.val());
        //za onemoguciti da netko upise sve 0
        var jeLiSveNula = brojInput.val().replace(/0{2,}/g, "0").trim();


        //mora imati ili 10 ili 13 znamenki i mora zapoceti sa +XXX ili 0XXXX...
        var brojRegex = /^(?:\+|0)[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{3,6}$/;
        if (brojInput.val().match(brojRegex) && (brojInput.val().length == 10 || brojInput.val().length == 13) && !(jeLiSveNula.length === 1)) {
            brojInput.removeClass("is-invalid");
            brojInput.addClass("is-valid");
        }
        else {
            brojInput.removeClass("is-valid");
            brojInput.addClass("is-invalid");
            $(`#brojMobitelaValidacija`).html("Potrebno je upisati broj oblika 0991234567 ili +385991234567");
        }
        if (slobodanBroj !== uniqueID && slobodanBroj !== false) {
            brojInput.removeClass("is-valid");
            brojInput.addClass("is-invalid");
            $(`#brojMobitelaValidacija`).html("Taj broj mobitela je već zauzet");
        }
        updateSubmitButtonStatus();
    });

});

function validateInputJQuery(input) {
    if (input.isValid()) {
        input.removeClass("is-invalid");
        input.addClass("is-valid");
        return true;
    } else {
        input.removeClass("is-valid");
        input.addClass("is-invalid");
        return false;
    }
}

// Provjera je li broj dostupan ili je već zauzet
// Ako je broj zauzet, vraća uniqueID osobe koja ima taj broj
function provjeriIdenticanBroj(broj) {
    var formatiraniBroj = "";
    //ako je oblika +XXX pretvori ga u 0XXXX
    if (broj.length === 13) {
        formatiraniBroj = broj.replace(/^\+(\d{3})(\d{3})(\d{6})/, "0$2$3");
    }
    for (let i = 0; i < localStorage.length; i++) {
        const key = localStorage.key(i);
        const value = JSON.parse(localStorage.getItem(key));
        var upisaniFormatiraniBroj;
        if (value.broj.length === 13) {
            upisaniFormatiraniBroj = value.broj.replace(/^\+(\d{3})(\d{3})(\d{6})/, "0$2$3");
        }
        if (value.broj === broj || value.broj === formatiraniBroj || upisaniFormatiraniBroj === broj || upisaniFormatiraniBroj == formatiraniBroj) {
            return value.uniqueID;
        }
    }
    return false;
}

//ako postoji barem 1 polje sa klasom "is-invalid" vrati false
//inace vrati true
function areAllInputsValid() {
    for (const inputField of inputFields) {
        if ($(inputField).hasClass('is-invalid')) {
            return false;
        }
    }
    return true;
}

function updateSubmitButtonStatus() {
    spremiPromjene.prop('disabled', !areAllInputsValid());
}

inputFields.on('input blur', updateSubmitButtonStatus);

//ako je email slobodan, vraca false
//ako je zauzet, vraca uniqueid osobe koja ima taj mail
function provjeriEmailDostupnost(emailValue) {
    var i, n = localStorage.length, keys = Object.keys(localStorage);
    for (i = 0; i < n; i++) {
        const osoba = JSON.parse(localStorage.getItem(keys[i]));
        if (emailValue === osoba.email)
            return osoba.uniqueID;
    }
    return false;
}

//uredi adresu i provjeri zadovoljava li uvjete
//uvjeti = minimalno 2 slova prije i poslije zareza, takoder obavezan zarez
//oblik je "ulica, grad"
function provjeriAdresu(adresa) {
    var returnValue = [true, ""];
    for (var i = 0; i < adresa.length; i++) {
        adresa[i] = adresa[i].replace(/ {2,}/g, " ").trim();

        if (adresa[i].length === 0)
            returnValue[0] = false;
        else
            returnValue[1] += adresa[i]

        if (i === 0) {
            returnValue[1].trim();
            returnValue[1] += ", ";
        }
    }
    return returnValue;
}