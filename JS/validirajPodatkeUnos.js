$.fn.isValid = function () {
  return this[0].checkValidity()
}

const saveButton = $(`#upisiDjelatnika`);
const myForm = $(`#forma`);
const storage = window.localStorage;
const inputFields = myForm.find(`.form-control`);

function showToast() {
  const toast = new bootstrap.Toast($(`#myToast`));
  toast.show();

  setTimeout(function () {
    toast.hide();
  }, 2500);

  isprazniFormu();
};

function isprazniFormu() {
  $(`#ime`).val("");
  $(`#prezime`).val("");
  $(`#email`).val("");
  $(`#adresa`).val("");
  $(`#brojMobitela`).val("");
  saveButton.prop('disabled', true);
};

saveButton.on("click", function () {
  if (window.confirm("Jeste li sve podatke dobro upisali?")) {

    const ime = $(`#ime`).val().replace(/ {2,}/g, " ").trim();
    const prezime = $(`#prezime`).val().replace(/ {2,}/g, " ").trim();
    const email = $(`#email`).val().trim();
    const adresa = $(`#adresa`).val().replace(/ {2,}/g, " ").replace(/ ,/g, ",").trim();
    const broj = $(`#brojMobitela`).val();
    const uniqueID = uuid.v4();

    const osoba = {
      ime,
      prezime,
      email,
      adresa,
      broj,
      uniqueID
    };

    const osobaJSON = JSON.stringify(osoba);
    localStorage.setItem(uniqueID, osobaJSON);
    showToast();
    $('.is-valid').removeClass('is-valid');
    isprazniFormu();
  }
});


// TRENUTNA VALIDACIJA POLJA
const imeInput = $(`#ime`);
const prezimeInput = $(`#prezime`);
const emailInput = $(`#email`);
const adresaInput = $(`#adresa`);
const brojInput = $(`#brojMobitela`);

imeInput.on("input blur", function () {
  const regexBroj = /\d/;
  // zamijeni dvostruke razmake sa jednostrukim i makni razmak na pocetku i kraju imena
  const imeValue = imeInput.val().replace(/ {2,}/g, " ").trim();
  validateInputJQuery(imeInput)

  if (regexBroj.test(imeValue)) {
    $(`#imeValidation`).html("Potrebno je upisati ime bez znamenki");
    imeInput.addClass("is-invalid");
    imeInput.removeClass("is-valid");
  }
  else {
    $(`#imeValidation`).html("Potrebno je upisati ime sa barem 2 slova");
  }
  //u slucaju da su upisani razmaci pa da se to ne prihvati
  if (imeValue.length === 0) {
    $(`#imeValidation`).html("Potrebno je upisati ime sa barem 2 slova");
    imeInput.addClass("is-invalid");
    imeInput.removeClass("is-valid");
  }
  updateSubmitButtonStatus();
});

prezimeInput.on("input blur", function () {
  const regexBroj = /\d/;
  const prezimeValue = prezimeInput.val().replace(/ {2,}/g, " ").trim();
  validateInputJQuery(prezimeInput);

  if (regexBroj.test(prezimeValue)) {
    prezimeInput.addClass("is-invalid");
    prezimeInput.removeClass("is-valid");
    $(`#prezimeValidacija`).html("Potrebno je upisati prezime bez znamenki");
  }
  else {
    $(`#prezimeValidacija`).html("Potrebno je upisati prezime sa barem 2 slova");
  }
  if (prezimeValue.length === 0) {
    $(`#prezimeValidation`).html("Potrebno je upisati ime sa barem 2 slova");
    prezimeInput.addClass("is-invalid");
    prezimeInput.removeClass("is-valid");
  }
  updateSubmitButtonStatus();
});

emailInput.on("input blur", function () {
  var validRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
  var emailValue = emailInput.val();

  if (provjeriEmailDostupnost(emailValue) === false) {
    if (emailInput.isValid() === false || !emailValue.match(validRegex)) {
      $("#emailAdresa").html("Potrebno je upisati adresu oblika ime@domena");
    }
    validateInputJQuery(emailInput);
  }
  else {
    emailInput.removeClass("is-valid");
    emailInput.addClass("is-invalid");
    $("#emailAdresa").html("Ta email adresa je već zauzeta");
  }
  updateSubmitButtonStatus();
});

adresaInput.on("input blur", function () {
  //format oblika "ulica, grad" - zarez i minimalno 2 slova prije i poslije zareza su obavezni
  const regexAdresa = /^([\p{L}\d\s]{2,}),\s?([\p{L}\d\s]{2,})$/u;
  var adresaValue = adresaInput.val().split(",");

  var vrijediLiAdresa = provjeriAdresu(adresaValue);

  if (validateInputJQuery(adresaInput) === false || !regexAdresa.test(adresaInput.val()) || vrijediLiAdresa[0] === false) {
    adresaInput.addClass("is-invalid");
    adresaInput.removeClass("is-valid");
    $(`#adresaValidacija`).html("Potrebno je upisati adresu oblika \"Ulica, Grad\" i mora imati najmanje 4 znaka");
  }
  updateSubmitButtonStatus();
});

brojInput.on("input blur", function () {
  //mora imati ili 10 ili 13 znamenki i mora zapoceti sa +XXX ili 0XXXX...
  var brojRegex = /^(?:\+|0)[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{3,6}$/;
  //za onemoguciti da netko upise sve 0
  var jeLiSveNula = brojInput.val().replace(/0{2,}/g, "0").trim();

  if (brojInput.val().match(brojRegex) && (brojInput.val().length == 10 || brojInput.val().length == 13) && !(jeLiSveNula.length === 1)) {
    brojInput.removeClass("is-invalid");
    brojInput.addClass("is-valid");
  }
  else {
    brojInput.removeClass("is-valid");
    brojInput.addClass("is-invalid");
    $(`#brojMobitelaValidacija`).html("Potrebno je upisati broj oblika 0991234567 ili +385991234567");
  }
  if (provjeriIdenticanBroj(brojInput.val())) {
    brojInput.removeClass("is-valid");
    brojInput.addClass("is-invalid");
    $(`#brojMobitelaValidacija`).html("Taj broj mobitela je već zauzet");
    // ovo je jako loše iz sigurnosnog aspekta!
  }
  updateSubmitButtonStatus();
});

function validateInputJQuery(input) {
  if (input.isValid()) {
    input.removeClass("is-invalid");
    input.addClass("is-valid");
    return true;
  } else {
    input.removeClass("is-valid");
    input.addClass("is-invalid");
    return false;
  }
}

// Provjera je li broj dostupan ili je već zauzet
// vraća true ako je taj broj već zauzet
function provjeriIdenticanBroj(broj) {
  var formatiraniBroj = "";
  if (broj.length === 13) {
    formatiraniBroj = broj.replace(/^\+(\d{3})(\d{3})(\d{6})/, "0$2$3");
  }
  for (let i = 0; i < localStorage.length; i++) {
    const key = localStorage.key(i);
    const value = JSON.parse(localStorage.getItem(key));
    var upisaniFormatiraniBroj;
    if (value.broj.length === 13) {
      upisaniFormatiraniBroj = value.broj.replace(/^\+(\d{3})(\d{3})(\d{6})/, "0$2$3");
    }
    if (value.broj === broj || value.broj === formatiraniBroj || upisaniFormatiraniBroj === broj || upisaniFormatiraniBroj == formatiraniBroj) {
      return true;
    }
  }
  return false;
}

//ako postoji barem 1 polje sa klasom "is-invalid" vrati false
//inace vrati true
function areAllInputsValid() {
  for (const inputField of inputFields) {
    if (!$(inputField).hasClass('is-valid')) {
      return false; // At least one input field is not valid
    }
  }
  return true; // All input fields have the "is-valid" class
}

function updateSubmitButtonStatus() {
  saveButton.prop('disabled', !areAllInputsValid());
}

// Add event listeners to the input fields to check validity on input change
inputFields.on('input', updateSubmitButtonStatus);
inputFields.on('blur', updateSubmitButtonStatus);

$(document).ready(function () {
  // Call the function initially to set the initial state of the submit button
  updateSubmitButtonStatus();
})

//ako je email slobodan, vraca false
//ako je zauzet, vraca uniqueid osobe koja ima taj mail
function provjeriEmailDostupnost(emailValue) {
  var i, n = localStorage.length, keys = Object.keys(localStorage);

  for (i = 0; i < n; i++) {
    const osoba = JSON.parse(localStorage.getItem(keys[i]));
    if (emailValue === osoba.email)
      return true;
  }
  return false;
}

//uredi adresu i provjeri zadovoljava li uvjete
//uvjeti = minimalno 2 slova prije i poslije zareza, takoder obavezan zarez
//oblik je "ulica, grad"
function provjeriAdresu(adresa) {
  var returnValue = [true, ""];
  for (var i = 0; i < adresa.length; i++) {
    adresa[i] = adresa[i].replace(/ {2,}/g, " ").trim();

    if (adresa[i].length === 0)
      returnValue[0] = false;
    else
      returnValue[1] += adresa[i]

    if (i === 0) {
      returnValue[1].trim();
      returnValue[1] += ", ";
    }
  }
  return returnValue;
}