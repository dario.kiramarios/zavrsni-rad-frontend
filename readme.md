**Zavrsni rad**

Ovo je napravio Dario Kiramarios za interni projekt.

Stranica ima mogućnosti kreiranja, uređivanja i brisanja djelatnika.
Testirana je na 3 glavne širine: 320px (mali mobitel), 768px (tablet), 1200 (monitor).
Također je napravljena responzivno tako da normalno radi i na drugim širinama između 320px i 1440px.

Bitna napomena je da sam koristio ***DataTables*** (https://datatables.net/) koji ima mogućnosti prikaza, sortiranja i filtriranja podataka.

Kako nemam backend, on je simuliran sa LocalStorage-om pa se svi podaci spremaju i uzimaju iz LocalStorage-a.

Pri kreiranju djelatnika im se pridruzuje uniqueID koji je zapravo uuid i pridodaju im se ostalo upisani podaci.

Takoder se koristi Bootstrap 5 (https://getbootstrap.com/).

***Validacija***

Pri kreiranju ili uređivanju podataka, provodi se validacija podataka.
Za ime i prezime je bitno da nema brojki i da ima minimalno 2 znaka (razmaci na početku i kraju imena se brišu).

Za email je preuzet regex sa interneta gdje je format oblika ime@domena, takoder ne smije biti razmaka u email adresi.
Također se pri upisivanju/uređivanju provjerava je li ta email adresa već zauzeta ili ne tj. je li dostupna ili ne.

Za adresu je format "ulica, grad" gdje je potrebno upisati minimalno 2 slova s lijeve i desne strane zareza te je sam zarez bitan za upisati.

Za broj mobitela je format +XXX i onda 9 znamenki ili 0XXX tako da su jedino formati sa 10 i 13 znakova prihvatljivi.
Slova su zabranjena, ali zagrade su prihvatljive zbog npr. americkih brojeva.
Također se provjarava je li taj broj već zauzet ili ne (neovisno o tome bio on u formatu +XXX... ili 0...)
